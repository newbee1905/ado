#ifndef SESSIONS_H
#define SESSIONS_H

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

int getpstartts(int pid, unsigned long long *startts);
int getsession(int pid, unsigned int ts_ttl);

#endif // SESSIONS_H
