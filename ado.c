#include <bsd/readpassphrase.h>
#include <crypt.h>
#include <err.h>
#include <pwd.h>
#include <shadow.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sessions.h"

#define VERSION "1.3"

void getconf(FILE *fp, const char *entry, char *result, size_t len_result) {
  char *line = NULL;
  size_t len = 0;

  fseek(fp, 0, SEEK_SET);

  while (getline(&line, &len, fp) != -1) {
    if (strncmp(entry, line, strlen(entry)) == 0) {
      strtok(line, "=");
      char *token = strtok(NULL, "=");
      if (token) {
        strncpy(result, token, len_result);
        result[strcspn(result, "\n")] = 0;
        free(line);
        return;
      }
    }
  }

  errx(1, "Could not get '%s' entry in config", entry);
}

int runprog(int argc, char **argv) {
  for (int i = 0; i < argc; i++)
    argv[i] = argv[i + 1];

  if (setuid(0) < 0)
    err(1, "Could not setuid");
  if (setgid(0) < 0)
    err(1, "Could not setgid");

  if (execvp(argv[0], argv) != 0)
    perror(argv[0]);

  return 0;
}

int main(int argc, char **argv) {
  char username[64], wrong_pw_sleep[64], session_ttl[64], password[128];
  unsigned int sleep_ms, tries, ts_ttl;

  if (argc == 1) {
    printf("RootDO version: %s\n\n", VERSION);
    printf("Usage: %s [command]\n", argv[0]);
    return 0;
  }

  int ruid = getuid();
  if (ruid == 0)
    return runprog(argc, argv);

  FILE *fp = fopen("/etc/ado.conf", "r");

  if (!fp)
    err(1, "Could not open /etc/ado.conf");

  getconf(fp, "username", username, sizeof(username));
  getconf(fp, "wrong_pw_sleep", wrong_pw_sleep, sizeof(wrong_pw_sleep));
  getconf(fp, "session_ttl", session_ttl, sizeof(session_ttl));
  sleep_ms = atoi(wrong_pw_sleep) * 1000;
  ts_ttl = atoi(session_ttl) * 60;

  fclose(fp);

  if (getsession(getppid(), ts_ttl) == 0)
    return runprog(argc, argv);

  struct passwd *p = getpwnam(username);
  if (!p)
    err(1, "Could not get user info");

  int uid = p->pw_uid;
  if (uid != ruid && ruid != 0)
    errx(1, "You are not in the username file");

  struct spwd *shadowEntry = getspnam(username);

  if (!shadowEntry)
    err(1, "Could not get shadow entry");

  tries = 0;
  while (tries < 3) {
    if (!readpassphrase("(ado) Password: ", password, sizeof(password),
                        RPP_REQUIRE_TTY))
      err(1, "Could not get passphrase");

    int rc =
        strcmp(shadowEntry->sp_pwdp, crypt(password, shadowEntry->sp_pwdp));
    memset(password, 0, sizeof(password));

    if (rc == 0) {
      setsession(getppid(), ts_ttl);
      return runprog(argc, argv);
    }

    usleep(sleep_ms);
    fprintf(stderr, "Wrong password.\n");
    tries++;
  }
  errx(1, "Too many wrong password attempts.");
  return 1;
}
