CC = gcc

CFLAGS  = -D_FORTIFY_SOURCE=2 -Ofast -flto -g -Wall -Wextra
LDFLAGS = -lcrypt -lbsd

SRC = sessions.c ado.c
OBJ = ${SRC:.c=.o}

all: ado

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: sessions.h

ado: ${OBJ}
	$(CC) -o $@ $(OBJ) ${CFLAGS} ${LDFLAGS}

install: ado
	cp ado /usr/bin/ado
	chown root:root /usr/bin/ado
	chmod 755 /usr/bin/ado
	chmod u+s /usr/bin/ado
	cp ado_sample.conf /etc/ado.conf

uninstall:
	rm /usr/bin/ado
	rm /etc/ado.conf

clean:
	rm -f ado ${OBJ}
