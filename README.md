# AdminDo

I am just a Uni student who wants to learn more about Linux, C and C++ and is obsessed with speed. This is just a fun project that I plan to fork and change for learning.

### Installation

```sh
git clone https://codeberg.org/newbee1905/ado
cd ado
make
sudo make install
```

After that, you'll have to configure ado to allow you to use it.
To do this, edit `/etc/ado.conf`, and set the username variable to your own.

After that you're good to go!

And to uninstall:

```sh
sudo make uninstall
```

### Usage

```sh
ado [command]
```

The configuration file has the following variables:
```
username=newbee
wrong_pw_sleep=1000
session_ttl=5
```

- `username`: The username of the user that is allowed to execute rdo (no multi user or group support (yet)).
- `wrong_pw_sleep`: The amount of milliseconds to sleep at a wrong password attempt. Must be a positive integer. Set to 0 to disable.
- `session_ttl`: The amount of minutes a session lasts. Must be a positive integer. Set to 0 to disable.

### Dependencies
- `libbsd`

### Benchmarks

The benchmark: Execute `whoami` (GNU coreutils 9.0) 1000 times.

|Program|Time|
--- | --- 
sudo 1.9.9 | 13.62s
rdo 1.2 | 1.02s
ado 1.2 | 1.02s

Baseline here is how long it took without any wrapper to make it root.

These benchmarks were done on a single core of an `Intel I7-9750H` Laptop processor, on Arch Linux `5.16.7-xanmod1-rog-1`

The benchmark is not very consistent but it can represent somewhat of the difference between sudo and rdo/ado

`sudo` and `opendoas` were pulled from the pacman repos, rdo via AUR.

All configs were kept as default, except allow the `wheel` group on both + enable `persist` on doas.

Script used:
```sh
#!/bin/sh

$1 whoami

current=$(date +%s.%N)
for i in {1..1000}; do
	$1 whoami 2>&1 >/dev/null
done
done=$(date +%s.%N)

echo $done - $current | bc
```

The script requires `bc` to be installed, for floating point arithmetics.

### TODO

- [x] Rename the project to ado
- [x] Change a few things to my liking
- [ ] Convert it to C++
- [ ] Try to make it as fast as possible for no reasons

### Acknowledgements

- Fork from [RootDO](https://codeberg.org/sw1tchbl4d3/rdo) [![AUR](https://img.shields.io/aur/version/rdo.svg)](https://aur.archlinux.org/packages/rdo/)
